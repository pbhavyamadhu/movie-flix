import React from 'react';
import {Component} from 'react';
import {getMovies} from '../services/fakeMovieService';
import Like from './Common/Like';
import Pagination from './Common/Pagination';
import {paginate} from './Utils/paginate';

class Movies extends Component {
	state = {
		movies: getMovies(),
		pageSize: 4,
		currentPage: 1,
	};

	handleLike = (movie) => {
		const movies = [...this.state.movies];
		const index = movies.indexOf(movie);
		movies[index] = {...movies[index]};
		movies[index].liked = !movies[index].liked;
		this.setState({movies});
	};

	handleDelete = (movie) => {
		const movies = this.state.movies.filter((m) => m._id !== movie._id);
		this.setState({movies});
	};

	handlePageChange = (page) => {
		this.setState({currentPage: page});
	};

	render() {
		const {length} = this.state.movies;
		const {movies: allMovies, pageSize, currentPage} = this.state;

		if (length === 0) return <h4>There are no movies in the database</h4>;

		const movies = paginate(allMovies, currentPage, pageSize);
		return (
			<React.Fragment>
				<h4>Showing {length} movies in the database</h4>
				<main className="container">
					<table className="table table">
						<thead>
							<tr>
								<th>Movie Title</th>
								<th>Genre</th>
								<th>Stocks</th>
								<th>Rental Rate</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{movies.map((movie) => (
								<tr key={movie._id}>
									<td>{movie.title}</td>
									<td>{movie.genre.name}</td>
									<td>{movie.numberInStock}</td>
									<td>{movie.dailyRentalRate}</td>
									<td>
										<Like
											liked={movie.liked}
											onLike={() => {
												this.handleLike(movie);
											}}
										/>
									</td>
									<td>
										<button
											className="btn btn-danger"
											onClick={() => this.handleDelete(movie)}
										>
											Delete
										</button>
									</td>
								</tr>
							))}
						</tbody>
					</table>
					<Pagination
						itemsCount={length}
						currentPage={currentPage}
						pageSize={pageSize}
						onPageChange={this.handlePageChange}
					/>
				</main>
			</React.Fragment>
		);
	}
}

export default Movies;
