import React from 'react';
import _ from 'lodash';

const Pagination = (props) => {
	const {pageSize, itemsCount, onPageChange, currentPage} = props;
	const pageCount = Math.ceil(itemsCount / pageSize);
	const pages = _.range(1, pageCount + 1);
	if (pages == 1) return null;

	return (
		<div>
			<nav aria-label="Page navigation example">
				<ul className="pagination">
					{pages.map((page) => (
						<li
							className={
								currentPage === page ? 'page-item active' : 'page-item'
							}
							style={{cursor: 'pointer'}}
							key={page}
							onClick={() => onPageChange(page)}
						>
							<a className="page-link">{page}</a>
						</li>
					))}
				</ul>
			</nav>
		</div>
	);
};

export default Pagination;
