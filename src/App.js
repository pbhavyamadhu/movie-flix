import Movies from './Components/Movies';
import './App.css';

function App() {
	return (
		<div>
			<main className="container">
			<h1>Movies Component</h1>
			<Movies />
			</main>
		</div>
	);
}

export default App;
